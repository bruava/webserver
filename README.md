# Web Server

Basic Web Server using sockets and serving web pages. It only response to GET 
requests, but allows multiple connections.

# Deploy

Run it on a Java IDE. By default it listens on port 8080. Only as two default 
pages, Index "/" and "/404" page not found.

## Usage

Can serve any pages you want if added to content folder and inserted in the 
Enum class