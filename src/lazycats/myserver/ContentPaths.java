package lazycats.myserver;

public enum ContentPaths {

    INDEX("./contents/Index.html"),
    ERROR404("./contents/Error404.html"),
    BATATA("./contents/Batata.jpg"),
    ERROR404PICTURE("./contents/Error404Picture.jpg");

    private String path;

    ContentPaths(String path){

        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
