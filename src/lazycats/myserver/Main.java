package lazycats.myserver;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {


    public static void main(String[] args) {



        try {

            ServerSocket myServerSocket = new ServerSocket(WebServer.PORT);

            /**
             * While no user's we wait above this code line
             */


            while(true) {


                /**
                 * Accepting the client connection creates a new socket to manage its demand
                 */

                WebServer myServer = new WebServer(myServerSocket.accept());


                Thread thread = new Thread(myServer);
                thread.start();

           }


        }catch (IOException ex){
            System.err.println("Error: Server connection error" + ex.getMessage());
        }

    }
}
