package lazycats.myserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer implements Runnable {

    static final int PORT = 8080;

    private Socket clientSocket;


    /**
     *
     * @param clientSocket each client as is individual socket
     */

    public WebServer(Socket clientSocket){

        this.clientSocket = clientSocket;
    }

    /**
     * Thread for dealing with each individual client method starts all methods chain
     */

    @Override
    public void run() {

        System.out.println(Thread.currentThread());

        start();
    }

    /**
     * Start Method concerns with receiving and handling the client request
     */

    private void start() {

        String headReceivedFromCliente;
        String verb;
        String url;
        String mimeType;
        String solicitedURLTruePath;


        try {

            /**
             * Creates a Server Socket and accepts client connection
             */

            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());

            /**
             * Receives the client header request
             */

            headReceivedFromCliente = receiveHeader(inFromClient);

            /**
             * Interpreter of client demand reading the client header request
             */

            verb = receiveRequestLineVerb(headReceivedFromCliente);
            url = requestedURL(headReceivedFromCliente);

            /**
             * Matches the solicited page from client with correct path URL
             */

            solicitedURLTruePath = urlPathReader(url);

            // System.out.println(solicitedURLTruePath);

            mimeType = getContentType(solicitedURLTruePath);

            responder(solicitedURLTruePath, verb, mimeType, outToClient);

            closeAllPipesAndSockets(outToClient, inFromClient, clientSocket);

        } catch (IOException ex) {

            System.err.println("Error: Could not connect ");
        }

    }

    /**
     * This methods receive client requests and processes them to redirecting the solicited url and verb
     */

    private String receiveHeader(BufferedReader inFromClient){

        String headReader;

        try {

            headReader = inFromClient.readLine();

            // System.out.println(headReader); // Only to see if properly delivered

            return headReader;

        } catch (IOException ex){

            System.err.println("Error: Could not receive client header" + ex.getMessage());
        }
        return null;
    }

    private String receiveRequestLineVerb(String header){

        String[] verbAnalyzer;

        verbAnalyzer = header.split(" ");

        // System.out.println(verbAnalyzer[0]); // Only to see if properly delivered

        return verbAnalyzer[0];
    }

    private String requestedURL(String header){

        String[] uRLInterpreter;

        uRLInterpreter = header.split(" ");

        // System.out.println(uRLInterpreter[1]); // Only to see if properly delivered

        return uRLInterpreter[1];
    }

    /**
     * Reads the URL and gets it correct path
     * @param url returns the URL analyzed by the requestedURL method
     * @return returns the desired page path
     */

    private String urlPathReader(String url){

        switch (url){

            case "/":
                return ContentPaths.INDEX.getPath();

            case "/batata.jpg":
                return ContentPaths.BATATA.getPath();

            case "/Error404Picture.jpg":
                return ContentPaths.ERROR404PICTURE.getPath();
        }

        return ContentPaths.ERROR404.getPath();
    }


    /**
     * @param requestedUrl this method analyzes the URL to return the mime type
     * @return the content type of the requested URL
     */

    private String getContentType(String requestedUrl){

        String[] whichMimeType;

        whichMimeType = requestedUrl.split("\\.");

        String mimeTypeToReturn = "text.html";

        // System.out.println("\n");
        // System.out.println(whichMimeType[2]); // Only to see if properly delivered


        switch (whichMimeType[2]){

            case "html":

                mimeTypeToReturn = "text/html";
                break;

            case "css":
                mimeTypeToReturn = "text/css";
                break;

            case "jpg":

                mimeTypeToReturn = "image/jpeg";
                break;

            case "png":

                mimeTypeToReturn = "image/png";
                break;

            case "gif":

                mimeTypeToReturn = "image/gif";
                break;

            case "mpeg":

                mimeTypeToReturn = "audio/mpeg";
                break;
        }

        // System.out.println(mimeTypeToReturn); // Only to see if properly delivered
        // System.out.println("\n");

        return mimeTypeToReturn;
    }

    /**
     * This method is called after handling and interpreting the client demand to response to the request
     * @param requestedURL path to what was asked
     * @param verb action received from client // Not yet implemented
     * @param mimeType file type
     * @param outToClient Data Output Stream to send header and files to client
     */

    private void responder(String requestedURL, String verb, String mimeType, DataOutputStream outToClient){

        int fileLength;

        //TODO: Implement the If verb != GET condition

        /**
         * Opens and reads the file size
         */

        File requestedFile = new File(requestedURL);

        fileLength = (int) requestedFile.length();

        // System.out.println("FILE SIZE:" + fileLength + "\n"); // Only to see if properly delivered

        /**
         * Sends the status header upon requested file
         */

        sendStatusHeader(mimeType, fileLength, outToClient);

        streamFile(outToClient, requestedFile);

    }

    /**
     * Gets all the information and compiles it in a header. After that, sends it
     * @param contentType mime type
     * @param contentLength length in integer
     * @param headerOutToClient Output Stream to send header to client
     */

    private void sendStatusHeader(String contentType, int contentLength, DataOutputStream headerOutToClient){

        try{

            String header ="HTTP/1.1 200 Document Follows\n"
                    + "Content-Type: " + contentType
                    + "\n" + "Content-Length: " + contentLength + "\n\n";

            // System.out.println(header);

            headerOutToClient.writeBytes(header);

        } catch (IOException ex){

            System.err.println("Error: Header didn't reach client" + ex.getMessage());
        }
    }

    /**
     * This method is responsible for streaming the content
     * @param outToClient Output Stream
     * @param requestedFile File to be sent
     */

    private void streamFile(OutputStream outToClient, File requestedFile){

        byte[] fileData = new byte[1024];

        try {

            FileInputStream inFromFile = new FileInputStream(requestedFile);

            int numbOfBytes;

            while ((numbOfBytes = inFromFile.read(fileData)) != -1){
                outToClient.write(fileData, 0, numbOfBytes);
            }

            inFromFile.close();

        } catch (FileNotFoundException ex){

            System.err.println("Error: File Not Found " + ex.getMessage());
        } catch (IOException ex){

            System.err.println("Error: Could not read file " + ex.getMessage());
        }

    }

    /**
     * After ending handling all request we close all pipes and the socket just to make sure
     * @param out
     * @param in
     * @param clientSocket
     */

    private void closeAllPipesAndSockets(DataOutputStream out, BufferedReader in, Socket clientSocket){

        try {

            out.close();
            in.close();
            clientSocket.close();
        } catch (IOException ex) {
            System.err.println("Error: Could not close Pipes or Socket " + ex.getMessage());
        }
    }
}
